"use strict";

module.exports = {

    ok(res, data) {
        res.status(200).json({
            data,
            success: true
        });
    },

    error(res, err) {
        res.status(err.status).send({
            "error": {
                code: err.code,
                message: err.message,
                stack: err.stack || '',
            },
            success: false
        });
    }
};