# cutomers-in-proximity Node.js Project

A Node.js project to read from a file of customers with information of their name,user_id and location coordinates and store them one by one in database(in this project MongoDB) and finally search the list of customers residing in the proximity of 100 km from coordinates of the Dublin.

# Instructions to install on local machine
1. Run Command - git clone https://gitlab.com/harsh-thakur/cutomers-in-proximity-node.js-project.git
2. Run npm install
3. npm run start


# To Retrieve the customers in the vicinity of 100 km of Dublin
Hit the URL in your browser - localhost:${portOnWhichYourLocalServerIsRunning}/api/v1/solution

To Start the Process afresh-
Hit the URL in your browser - localhost:${portOnWhichYourLocalServerIsRunning}/api/v1/seed

And hit the URl - localhost:${portOnWhichYourLocalServerIsRunning}/api/v1/solution
to see the solution

# NPM packages Used-
    "body-parser": "^1.19.0",
    "cors": "^2.8.5",
    "dotenv": "^8.0.0",
    "express": "^4.17.1",
    "mongoose": "^5.6.2"