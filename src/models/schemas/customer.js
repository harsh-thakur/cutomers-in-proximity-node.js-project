var mongoose = require('mongoose');
var Schema = mongoose.Schema;

const pointSchema = new mongoose.Schema({
    "type": {
        type: String,
        default:'Point'
    },
    "coordinates": {
        type: [Number],
        required: true
    }
});

var customerSchema = new Schema({
    "user_id":{
        type: Number,
        required:true,
        unique:true
    },
    "name":{
        type:String,
        required:true
    },
    "location": {
        type: pointSchema,
        required: true
    }
});

customerSchema.index({ location: '2dsphere' });
module.exports = customerSchema;
