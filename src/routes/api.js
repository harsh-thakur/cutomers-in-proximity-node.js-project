const express = require('express');
const router = express.Router();
const response = require('./../middlewares/response');
const seed = require('./../controllers/seed');
const query = require('./../controllers/query');



router.get('/', function (req, res) {
    response.ok(res,'1.0');
});


router.get('/seed',seed.readFileAndStore); // To seed initial data to Database
router.get('/solution', query.solution);   // To retrive list of customers in the proximity of 100 km from dublin.

module.exports = router;
