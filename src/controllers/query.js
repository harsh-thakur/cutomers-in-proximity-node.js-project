"use strict";
const Customer = require('./../models/customer');
const response = require('./../middlewares/response');


module.exports = {

    async solution(req, res) {
        try {
            let queryPipeline = [{
                $geoNear:{
                    near:{
                        type:"Point",
                        coordinates: [-6.257664, 53.339428]
                    },
                    distanceField: "dist.calculated",
                    maxDistance: 100000,
                    distanceMultiplier: 0.001,
                    includeLocs: "dist.location",
                    spherical: true
                }
            },{
                $project:{
                    "_id":0,
                    "user_id":1,
                    "name":1
                }
            },{
                $sort:{
                    'user_id':1
                }
            }];
        
        let filteredCustomers = await Customer.aggregate(queryPipeline);
        response.ok(res,filteredCustomers);

        } catch (error) {
            response.error(res, error);
        }
    }
}


//Error handling

process.on('uncaughtException', function (err) {
    console.log(err)
})
