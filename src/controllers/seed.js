"use strict";
const Customer = require('./../models/customer');
const fs = require('fs');
const response = require('./../middlewares/response');
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;


module.exports = {

  // Read the file and store in Customer collection.
   async readFileAndStore(req,res){
        try {
            await _resetDB();
            fs.readFile("customer.txt", "utf-8", async (err, data) => {
                if (err) {
                    response.error(res,err);   
                }
                
                let splitedDocuments = data.split('\n');
                
                let  listOfCutomers = splitedDocuments.map(JSON.parse);

                for (let customer of listOfCutomers) {
                    let customerObj = {
                         "user_id": customer.user_id,
                         "name": customer.name,
                         "location":{
                             "type": "Point",
                             "coordinates":[+customer.longitude,+customer.latitude]
                         }
                    }

                    await Customer.create(customerObj);
                }

                response.ok(res,'Customers information has been saved in DB successfully!.');
            });
                        
        } catch (error) {
         response.error(res,error);
        }
    }
}

// Reset database in case the collection alreay exists..
const _resetDB = async function () {

    return new Promise(function (resolve, reject) {
        const db = mongoose.connection;
        let droppedCollections = [];
        for (let collectionName in db.collections) {
            db.collections[collectionName].drop(function (err, reply) {
                droppedCollections.push(collectionName);
            })
        }
        setTimeout(resolve(droppedCollections), 3000);
    });
};
//Error handling

process.on('uncaughtException', function (err) {
    console.log(err)
})
